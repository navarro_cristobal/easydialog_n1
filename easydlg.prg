/*
    ==================================================================
    EasyDialog                                           Version 1.1.4
    ------------------------------------------------------------------
                                  (c) copyright: Timm Sodtalbers, 2002
                                                    Sodtalbers+Partner
                                              info@reportdesigner.info
                                               www.reportdesigner.info
    ==================================================================
*/

#INCLUDE "FiveWin.ch"
#INCLUDE "Struct.ch"

STATIC aShowCtrl, nHLightClr, nHLightWidth, cLongDlgIni

STATIC cED_Config  := ".\edsetup.ini"
STATIC nEDStyle    := 1
STATIC nPosSteps   := 4
STATIC nSizeSteps  := 4
STATIC lEDDemo     := .F.
STATIC lEDDemoMsg  := .F.
STATIC nLanguage   := 1
STATIC lResChanged := .F.

STATIC lxBrowse := .F.

MEMVAR cDlgIni

*-- FUNCTION -----------------------------------------------------------------
* Name........: ED_Use
* Beschreibung:
* Argumente...: None
* R�ckgabewert: .T.
* Author......: Timm Sodtalbers
*-----------------------------------------------------------------------------
FUNCTION ED_Use( aDlg, cOrigDlgIni, lAdd2SysMenu, aUserResName )

   LOCAL i, cOldDlgIni, oRect, aSize[2]

   DEFAULT cOrigDlgIni  := ".\EasyDlg.ini"
   DEFAULT lAdd2SysMenu := .T.
   DEFAULT aUserResName := ARRAY( LEN( aDlg ) )

   PUBLIC cDlgIni
   cOldDlgIni := cDlgIni

   aDlg := ED_aDlgCheck( aDlg )

   AEVAL( aDlg, {|x,y| ED_SetOneDlg( x, cOrigDlgIni,, aUserResName[y] ) } )

   aDlg[1]:bResized := {|| AEVAL( aDlg, {|x,y| ED_ResizeOneDlg( x,,, aUserResName[y] ) } ) }

   aDlg[1]:bMoved   := {|| ED_ResizeOneDlg( aDlg[1], .F., .T., aUserResName[1] ) }

   IF lAdd2SysMenu = .T.
      ED_Add2SysMenu( aDlg )
   ENDIF

   IF lResChanged = .T.
      //This adjusts the controls of a dialog when the resolution has changed
      oRect    := aDlg[1]:GetRect()
      aSize[1] := aDlg[1]:nWidth
      aSize[2] := aDlg[1]:nHeight
      aDlg[1]:Move( aDlg[1]:nTop-1, aDlg[1]:nLeft-1, aDlg[1]:nWidth-1, aDlg[1]:nHeight-1 )
      aDlg[1]:Move( oRect:nTop, oRect:nLeft, aSize[1], aSize[2], .T. )
      lResChanged := .F.
   ENDIF

RETURN ( cOldDlgIni )


*-- FUNCTION -----------------------------------------------------------------
* Name........: ED_SetDlg
* Beschreibung:
* Argumente...: None
* R�ckgabewert: .T.
* Author......: Timm Sodtalbers
*-----------------------------------------------------------------------------
FUNCTION ED_SetDlg( aDlg, cOrigDlgIni )

   LOCAL cOldDlgIni, oRect, aSize[2]

   DEFAULT cOrigDlgIni := ".\EasyDlg.ini"

   PUBLIC cDlgIni
   cOldDlgIni := cDlgIni

   aDlg := ED_aDlgCheck( aDlg )

   AEVAL( aDlg, {|x| ED_SetOneDlg( x, cOrigDlgIni ) } )

   IF lResChanged = .T.
      //This adjusts the controls of a dialog when the resolution has changed
      oRect    := aDlg[1]:GetRect()
      aSize[1] := aDlg[1]:nWidth
      aSize[2] := aDlg[1]:nHeight
      aDlg[1]:Move( aDlg[1]:nTop-1, aDlg[1]:nLeft-1, aDlg[1]:nWidth-1, aDlg[1]:nHeight-1 )
      aDlg[1]:Move( oRect:nTop, oRect:nLeft, aSize[1], aSize[2], .T. )
      lResChanged := .F.
   ENDIF

RETURN ( cOldDlgIni )


*-- FUNCTION -----------------------------------------------------------------
* Name........: ED_ResizeDlg
* Beschreibung:
* Argumente...: None
* R�ckgabewert: .T.
* Author......: Timm Sodtalbers
*-----------------------------------------------------------------------------
FUNCTION ED_ResizeDlg( aDlg, aUserResName )

   DEFAULT aUserResName := ARRAY( LEN( aDlg ) )

   aDlg := ED_aDlgCheck( aDlg )

   AEVAL( aDlg, {|x,y| ED_ResizeOneDlg( x,,, aUserResName[y] ) } )

RETURN (.T.)


*-- FUNCTION -----------------------------------------------------------------
* Name........: ED_aDlgCheck
* Beschreibung:
* Argumente...: None
* R�ckgabewert: .T.
* Author......: Timm Sodtalbers
*-----------------------------------------------------------------------------
FUNCTION ED_aDlgCheck( aDlg )

   LOCAL aNewDlg := {}

   IF VALTYPE( aDlg ) <> "A"
      AADD( aNewDlg, aDlg )
   ELSE
      aNewDlg := aDlg
   ENDIF

RETURN ( aNewDlg )


*-- FUNCTION -----------------------------------------------------------------
* Name........: ED_OnPaint
* Beschreibung:
* Argumente...: None
* R�ckgabewert: .T.
* Author......: Timm Sodtalbers
*-----------------------------------------------------------------------------
FUNCTION ED_OnPaint( oDlg, bAction )

   LOCAL i, y

   IF bAction <> NIL
      FOR y := 1 to Len( oDlg:aControls )
         EVAL( bAction, oDlg:aControls[y], oDlg )
      NEXT
   ENDIF

RETURN NIL


*-- FUNCTION -----------------------------------------------------------------
* Name........: ED_Restore
* Beschreibung:
* Argumente...: None
* R�ckgabewert: .T.
* Author......: Timm Sodtalbers
*-----------------------------------------------------------------------------
FUNCTION ED_Restore( cOldDlgIni )

   cDlgIni := cOldDlgIni

RETURN (.T.)


*-- FUNCTION -----------------------------------------------------------------
* Name........: ED_Add2SysMenu
* Beschreibung:
* Argumente...: None
* R�ckgabewert: .T.
* Author......: Timm Sodtalbers
*-----------------------------------------------------------------------------
FUNCTION ED_Add2SysMenu( aDlg )

   LOCAL i, oMenu, aMenu
   LOCAL nSelect  := SELECT()
   LOCAL lDevMode := IIF( GetPvProfString( "General", "DeveloperMode", "0", cED_Config ) = "1", .T., .F. )

   aDlg := ED_aDlgCheck( aDlg )

   aMenu := ARRAY( LEN(aDlg) )

   SELECT 0
   USE EASYDLG.DBF SHARED

   REDEFINE SYSMENU oMenu OF aDlg[1]

      SEPARATOR
      MENUITEM ED_GL( "&Design Dialog" )

      MENU

      FOR i := 1 TO LEN( aDlg )
         MENUITEM aMenu[i] PROMPT ALLTRIM(STR( i, 4 )) + ". " + ALLTRIM( aDlg[i]:cResName )
         aMenu[i]:bAction := {|x| ED_Setup( aDlg[VAL( x:cPrompt )] ) }
      NEXT

      IF lDevMode = .T.
         SEPARATOR
         MENUITEM ED_GL("Open") + " " + ALLTRIM( cLongDlgIni ) ;
            ACTION WINEXEC( "NOTEPAD.EXE " + ALLTRIM( cDlgIni ) )
         MENUITEM ED_GL("Copy the configuration file into the application directory") ;
            ACTION FileCopy( cLongDlgIni, ".\" + cFileNoPath( cLongDlgIni ) )
         SEPARATOR
         MENUITEM ED_GL("Open") + " " + "edsetup.ini" ;
            ACTION ( WAITRUN( "NOTEPAD.EXE edsetup.ini", 1 ), ED_GetConfig( "", .F. ) )
      ENDIF

      ENDMENU

   ENDSYSMENU

   EASYDLG->(DBCLOSEAREA())
   SELECT( nSelect )

RETURN (.T.)


*-- FUNCTION -----------------------------------------------------------------
* Name........: ED_SetCurrValues
* Beschreibung:
* Argumente...: None
* R�ckgabewert: .T.
* Author......: Timm Sodtalbers
*-----------------------------------------------------------------------------
FUNCTION ED_SetCurrValues( cDef, aCurrRect, aOrigRect )

   LOCAL aCurrControl[4]
   LOCAL nAdjustTop    := VAL( StrToken( cDef, 13, "|" ) )
   LOCAL nAdjustLeft   := VAL( StrToken( cDef, 14, "|" ) )
   LOCAL nAdjustWidth  := VAL( StrToken( cDef, 15, "|" ) )
   LOCAL nAdjustHeight := VAL( StrToken( cDef, 16, "|" ) )

   aCurrControl[1] := VAL( StrToken( cDef, 9, "|" ) ) + ;
                      IIF( nAdjustTop = 1, aCurrRect[4] - aOrigRect[4], 0 )
   aCurrControl[2] := VAL( StrToken( cDef, 10, "|" ) ) + ;
                      IIF( nAdjustLeft = 1, aCurrRect[3] - aOrigRect[3], 0 )
   aCurrControl[3] := VAL( StrToken( cDef, 11, "|" ) ) + ;
                      IIF( nAdjustWidth = 1, aCurrRect[3] - aOrigRect[3], 0 )
   aCurrControl[4] := VAL( StrToken( cDef, 12, "|" ) ) + ;
                      IIF( nAdjustHeight = 1, aCurrRect[4] - aOrigRect[4], 0 )

RETURN ( aCurrControl )


*-- FUNCTION -----------------------------------------------------------------
* Name........: ED_MoveDlg
* Beschreibung:
* Argumente...: None
* R�ckgabewert: .T.
* Author......: Timm Sodtalbers
*-----------------------------------------------------------------------------
FUNCTION ED_MoveDlg( aDlg, aUserResName )

   DEFAULT aUserResName := ARRAY( LEN( aDlg ) )

   aDlg := ED_aDlgCheck( aDlg )

   AEVAL( aDlg, {|x,y| ED_ResizeOneDlg( x, .F., .T., aUserResName[y] ) } )

RETURN (.T.)


*-- FUNCTION -----------------------------------------------------------------
* Name........: ED_GetLocalDlgIni
* Beschreibung:
* Argumente...: None
* R�ckgabewert: .T.
* Author......: Timm Sodtalbers
*-----------------------------------------------------------------------------
FUNCTION ED_GetLocalDlgIni( cOrigDlgIni )

   LOCAL cNewDlgIni, hFile, cShortDlgIni, cAppVersion, cShortNewDlgIni
   LOCAL cTmpPath    := ED_GetTempPath() + "\edialog"

   DEFAULT cOrigDlgIni := ".\EasyDlg.ini"

   cShortDlgIni := ED_LF2SF( cOrigDlgIni )

   IF SUBSTR( cShortDlgIni, LEN( cShortDlgIni ) ) = "\"
      hFile := lCreat( "EASYDLG.TMP", 0 )
      lClose( hFile )
      FileCopy( "EASYDLG.TMP", cOrigDlgIni )
      ED_DelFile( "EASYDLG.TMP" )
      cShortDlgIni := ED_LF2SF( cOrigDlgIni )
   ENDIF

   IF AT( "\", cOrigDlgIni ) = 0
      cNewDlgIni := cTmpPath + "\" + ALLTRIM( cOrigDlgIni )
   ELSE
      cNewDlgIni := cTmpPath + "\" + SUBSTR( cOrigDlgIni, RAT( "\", cOrigDlgIni ) + 1 )
   ENDIF

   IF lIsDir( cTmpPath ) = .F.
      lMKDir( cTmpPath )
   ENDIF

   IF FILE( ED_LF2SF( cNewDlgIni ) ) = .F.
      FileCopy( cShortDlgIni, cNewDlgIni )
   ENDIF

   cLongDlgIni     := cNewDlgIni
   cShortNewDlgIni := ED_LF2SF( cNewDlgIni )

   // if a configuration file with a different version string exists
   cAppVersion := GetPvProfString( "EasyDialogGeneral", "AppVersion", "", cShortDlgIni )

   IF EMPTY( cAppVersion ) = .F.
      IF ALLTRIM( cAppVersion ) == ;
         ALLTRIM( GetPvProfString( "EasyDialogGeneral", "AppVersion", "", cShortNewDlgIni ) )
      ELSE
         FileCopy( cShortDlgIni, cNewDlgIni )
      ENDIF
   ENDIF

RETURN ( cShortNewDlgIni )


*-- FUNCTION -----------------------------------------------------------------
* Name........: ED_SetOneDlg
* Beschreibung:
* Argumente...: None
* R�ckgabewert: .T.
* Author......: Timm Sodtalbers
*-----------------------------------------------------------------------------
FUNCTION ED_SetOneDlg( oDlg, cOrigDlgIni, lSetup, cUserResName )

   LOCAL i, cDef, aIniEntries, cDlgDef
   LOCAL nDlgTop, nDlgLeft, nDlgWidth, nDlgHeight
   LOCAL nCtrlTop, nCtrlLeft, nCtrlWidth, nCtrlHeight
   LOCAL cSepChar := "|"

   DEFAULT lSetup       := .F.
   DEFAULT cUserResName := oDlg:cResName

   IF lSetup = .F.
      ED_GetConfig( cOrigDlgIni )
   ENDIF

   cDlgDef := GetPvProfString( cUserResName, "Dialog", "", cDlgIni )

   IF EMPTY( cDlgDef )
      //First call: Read/write the default dialog setting
      ED_ResizeOneDlg( oDlg, .F.,, cUserResName )
      FileCopy( cDlgIni, cOrigDlgIni )
      RETURN (.T.)
   ENDIF

   IF VAL( StrToken( cDlgDef, 15, cSepChar ) ) = 1
      //when resolution changed
      lResChanged := .T.
   ENDIF

   aIniEntries := ED_GetIniSection( cUserResName, cDlgIni, .F. )

   IF LEN( oDlg:aControls ) > LEN( aIniEntries ) - 1
      //when controls were added
      ED_ResizeOneDlg( oDlg, .F.,, cUserResName )
      RETURN (.T.)
   ENDIF

   IF lEDDemo = .T. .AND. lEDDemoMsg = .F.
      ED_MsgDemo( "This application uses an unregistered" + CRLF + ;
                  "test version of EasyDialog." + CRLF + CRLF + ;
                  CHR(9) + CHR(9) + "Timm Sodtalbers" + CRLF + ;
                  CHR(9) + CHR(9) + "Sodtalbers+Partner" + CRLF + ;
                  CHR(9) + CHR(9) + "www.reportdesigner.info  ", ;
                  "Powered by EasyDialog!", 64 )
      lEDDemoMsg := .T.
   ENDIF

   IF lSetup = .F.

      nDlgTop    := VAL( StrToken( cDlgDef, 5, cSepChar ) )
      nDlgLeft   := VAL( StrToken( cDlgDef, 6, cSepChar ) )
      nDlgWidth  := VAL( StrToken( cDlgDef, 7, cSepChar ) )
      nDlgHeight := VAL( StrToken( cDlgDef, 8, cSepChar ) )

      IF StrToken( cDlgDef, 13, cSepChar ) = "1"
         nDlgLeft := GetSysMetrics( 0 ) / 2 - nDlgWidth / 2
      ENDIF
      IF StrToken( cDlgDef, 14, cSepChar ) = "1"
         nDlgTop := GetSysMetrics( 1 ) / 2 - nDlgHeight / 2
      ENDIF

      oDlg:Move( nDlgTop, nDlgLeft, nDlgWidth, nDlgHeight, .T. )

   ELSE

      EDTEMP->(DBAPPEND())

      REPLACE EDTEMP->TYPE  WITH "DIALOG", ;
              EDTEMP->ORIG1 WITH VAL( StrToken( cDlgDef, 1, cSepChar ) ), ;
              EDTEMP->ORIG2 WITH VAL( StrToken( cDlgDef, 2, cSepChar ) ), ;
              EDTEMP->ORIG3 WITH VAL( StrToken( cDlgDef, 3, cSepChar ) ), ;
              EDTEMP->ORIG4 WITH VAL( StrToken( cDlgDef, 4, cSepChar ) ), ;
              EDTEMP->CURR1 WITH VAL( StrToken( cDlgDef, 5, cSepChar ) ), ;
              EDTEMP->CURR2 WITH VAL( StrToken( cDlgDef, 6, cSepChar ) ), ;
              EDTEMP->CURR3 WITH VAL( StrToken( cDlgDef, 7, cSepChar ) ), ;
              EDTEMP->CURR4 WITH VAL( StrToken( cDlgDef, 8, cSepChar ) ), ;
              EDTEMP->MODY1 WITH VAL( StrToken( cDlgDef, 5, cSepChar ) ), ;
              EDTEMP->MODY2 WITH VAL( StrToken( cDlgDef, 6, cSepChar ) ), ;
              EDTEMP->MODY3 WITH VAL( StrToken( cDlgDef, 7, cSepChar ) ), ;
              EDTEMP->MODY4 WITH VAL( StrToken( cDlgDef, 8, cSepChar ) ), ;
              EDTEMP->ADJUST1 WITH IIF( StrToken( cDlgDef,  9, cSepChar ) = "1", .T., .F. ), ;
              EDTEMP->ADJUST2 WITH IIF( StrToken( cDlgDef, 10, cSepChar ) = "1", .T., .F.  ), ;
              EDTEMP->ADJUST3 WITH IIF( StrToken( cDlgDef, 11, cSepChar ) = "1", .T., .F.  ), ;
              EDTEMP->ADJUST4 WITH IIF( StrToken( cDlgDef, 12, cSepChar ) = "1", .T., .F.  ), ;
              EDTEMP->HCENTER WITH IIF( StrToken( cDlgDef, 13, cSepChar ) = "1", .T., .F.  ), ;
              EDTEMP->VCENTER WITH IIF( StrToken( cDlgDef, 14, cSepChar ) = "1", .T., .F.  )
   ENDIF

   FOR i := 1 to LEN( oDlg:aControls )

      cDef := ED_GetIniEntry( aIniEntries, ALLTRIM(STR( oDlg:aControls[i]:nID, 10 )), "" )

      IF lSetup = .F.

         nCtrlTop    := VAL( StrToken( cDef, 5, cSepChar ) )
         nCtrlLeft   := VAL( StrToken( cDef, 6, cSepChar ) )
         nCtrlWidth  := VAL( StrToken( cDef, 7, cSepChar ) )
         nCtrlHeight := VAL( StrToken( cDef, 8, cSepChar ) )

         IF StrToken( cDef, 17, cSepChar ) = "1"
            nCtrlLeft := nDlgWidth / 2 - nCtrlWidth / 2
         ENDIF
         IF StrToken( cDef, 18, cSepChar ) = "1"
            nCtrlTop := nDlgHeight / 2 - nCtrlHeight / 2
         ENDIF

         //IF UPPER( StrToken( cDef, 21, cSepChar ) ) = "TFOLDER" .OR. ;
         //   UPPER( StrToken( cDef, 21, cSepChar ) ) = "SYSTABCONTROL32"
         //   //Harbour/FWH: Using the move-method with folders causes an GPF
         //   MoveWindow( oDlg:aControls[i]:hWnd, ;
         //               nCtrlTop, nCtrlLeft, nCtrlWidth, nCtrlHeight, .T. )
         //   oDlg:aControls[i]:nTop    := nCtrlTop
         //   oDlg:aControls[i]:nLeft   := nCtrlLeft
         //   oDlg:aControls[i]:nBottom := nCtrlTop  + nCtrlWidth
         //   oDlg:aControls[i]:nRight  := nCtrlLeft + nCtrlHeight
         //ELSE
            oDlg:aControls[i]:Move( nCtrlTop, nCtrlLeft, nCtrlWidth, nCtrlHeight, .T. )
         //ENDIF

         IIF( StrToken( cDef, 19, cSepChar ) = "1", oDlg:aControls[i]:Disable(), oDlg:aControls[i]:Enable() )
         IIF( StrToken( cDef, 20, cSepChar ) = "1", oDlg:aControls[i]:Hide()   , oDlg:aControls[i]:Show()   )

         ED_AdjustBrowse( oDlg, cDef, i, VAL( StrToken( cDef, 3, cSepChar ) ) )

      ELSE

         EDTEMP->(DBAPPEND())
         REPLACE EDTEMP->ID      WITH oDlg:aControls[i]:nID, ;
                 EDTEMP->TYPE    WITH StrToken( cDef, 21, cSepChar ), ;
                 EDTEMP->ORIG1   WITH VAL( StrToken( cDef,  1, cSepChar ) ), ;
                 EDTEMP->ORIG2   WITH VAL( StrToken( cDef,  2, cSepChar ) ), ;
                 EDTEMP->ORIG3   WITH VAL( StrToken( cDef,  3, cSepChar ) ), ;
                 EDTEMP->ORIG4   WITH VAL( StrToken( cDef,  4, cSepChar ) ), ;
                 EDTEMP->CURR1   WITH VAL( StrToken( cDef,  5, cSepChar ) ), ;
                 EDTEMP->CURR2   WITH VAL( StrToken( cDef,  6, cSepChar ) ), ;
                 EDTEMP->CURR3   WITH VAL( StrToken( cDef,  7, cSepChar ) ), ;
                 EDTEMP->CURR4   WITH VAL( StrToken( cDef,  8, cSepChar ) ), ;
                 EDTEMP->MODY1   WITH VAL( StrToken( cDef,  9, cSepChar ) ), ;
                 EDTEMP->MODY2   WITH VAL( StrToken( cDef, 10, cSepChar ) ), ;
                 EDTEMP->MODY3   WITH VAL( StrToken( cDef, 11, cSepChar ) ), ;
                 EDTEMP->MODY4   WITH VAL( StrToken( cDef, 12, cSepChar ) ), ;
                 EDTEMP->ADJUST1 WITH IIF( StrToken( cDef, 13, cSepChar ) = "1", .T., .F. ), ;
                 EDTEMP->ADJUST2 WITH IIF( StrToken( cDef, 14, cSepChar ) = "1", .T., .F.  ), ;
                 EDTEMP->ADJUST3 WITH IIF( StrToken( cDef, 15, cSepChar ) = "1", .T., .F.  ), ;
                 EDTEMP->ADJUST4 WITH IIF( StrToken( cDef, 16, cSepChar ) = "1", .T., .F.  ), ;
                 EDTEMP->HCENTER WITH IIF( StrToken( cDef, 17, cSepChar ) = "1", .T., .F.  ), ;
                 EDTEMP->VCENTER WITH IIF( StrToken( cDef, 18, cSepChar ) = "1", .T., .F.  ), ;
                 EDTEMP->DISABLE WITH IIF( StrToken( cDef, 19, cSepChar ) = "1", .T., .F.  ), ;
                 EDTEMP->HIDE    WITH IIF( StrToken( cDef, 20, cSepChar ) = "1", .T., .F.  ), ;
                 EDTEMP->TEXT    WITH StrToken( cDef, 22, cSepChar ), ;
                 EDTEMP->BRWMODE WITH IIF( StrToken( cDef, 23, cSepChar ) = "1", .T., .F.  )

      ENDIF

   NEXT

RETURN (.T.)


*-- FUNCTION -----------------------------------------------------------------
* Name........: ED_AdjustBrowse
* Beschreibung:
* Argumente...: None
* R�ckgabewert: .T.
* Author......: Timm Sodtalbers
*-----------------------------------------------------------------------------
FUNCTION ED_AdjustBrowse( oDlg, cDef, i, nOldWidth )

   LOCAL y, cValue, nValue, aBrwCurSize, cMode
   LOCAL cAddDef := ""

   IF ED_IsBrowse( StrToken( cDef, 21, "|" ) ) = .T.

      cMode := ALLTRIM(StrToken( cDef, 23, "|" ))

      aBrwCurSize := IIF( lxBrowse, ED_xBrwGetColSizes( oDlg:aControls[i] ), ;
                                    oDlg:aControls[i]:GetColSizes() )
      FOR y := 1 TO LEN( aBrwCurSize )

         cValue := StrToken( cDef, 23+y, "|" )
         nValue := IIF( EMPTY( cValue ), aBrwCurSize[y], VAL( cValue ) )

         IF cMode = "1"
            aBrwCurSize[y] := nValue / nOldWidth * VAL( StrToken( cDef, 7, "|" ) )
            //aBrwCurSize[y] := aBrwCurSize[y] / nOldWidth * VAL( StrToken( cDef, 7, "|" ) )
         ELSE
            aBrwCurSize[y] := IIF( nValue = 0, oDlg:aControls[i]:aTmpColSizes[y], nValue )
         ENDIF

         cAddDef += ALLTRIM(STR( aBrwCurSize[y], 6 )) + "|"

      NEXT

      IF lxBrowse = .T.
         ED_xBrwSetColSizes( oDlg:aControls[i], aBrwCurSize )
      ELSE
         oDlg:aControls[i]:aColSizes := aBrwCurSize
      ENDIF

      cDef := SUBSTR( cDef, 1, ED_StrAtNum( "|", cDef, 22 ) ) + cMode + "|" + cAddDef

   ENDIF

RETURN ( cDef )


*-- FUNCTION -----------------------------------------------------------------
* Name........: ED_IsBrowse
* Beschreibung:
* Argumente...: None
* R�ckgabewert: .T.
* Author......: Timm Sodtalbers
*-----------------------------------------------------------------------------
FUNCTION ED_IsBrowse( cValue )

RETURN IIF( AT( "BROWSE", UPPER(ALLTRIM( cValue )) ) <> 0, .T., .F. )


*-- FUNCTION -----------------------------------------------------------------
* Name........: ED_GetConfig
* Beschreibung:
* Argumente...: None
* R�ckgabewert: .T.
* Author......: Timm Sodtalbers
*-----------------------------------------------------------------------------
FUNCTION ED_GetConfig( cOrigDlgIni, lSetDlgIni )

   DEFAULT lSetDlgIni := .T.

   nPosSteps  := VAL( GetPvProfString( "General", "PositionSteps", "4", cED_Config ) )
   nSizeSteps := VAL( GetPvProfString( "General", "SizeSteps"    , "4", cED_Config ) )

   nHLightClr   := ED_GetColor( GetPvProfString( "General", "HighlightColor", "0,128,192", cED_Config ) )
   nHLightWidth := VAL( GetPvProfString( "General", "HighlightWidth", "2", cED_Config ) )

   nLanguage := VAL( GetPvProfString( "General", "Language", "1", cED_Config ) )

   IF lSetDlgIni = .T.
      cDlgIni := ED_GetLocalDlgIni( cOrigDlgIni )
   ENDIF

RETURN (.T.)


*-- FUNCTION -----------------------------------------------------------------
* Name........: ED_Setup
* Beschreibung:
* Argumente...: None
* R�ckgabewert: .T.
* Author......: Timm Sodtalbers
*-----------------------------------------------------------------------------
FUNCTION ED_Setup( oAppDlg )

   LOCAL oDlg, oFld, i, oBrw, aBtn[2], aUpd[14], aCrtl[18]
   LOCAL hOldRes  := GetResources()
   LOCAL nSelect  := SELECT()
   LOCAL cChecked := "X    "
   LOCAL cDBF     := ED_GetTempPath() + "\EDIALOG\EDTEMP.DBF"

   #IFNDEF __HARBOUR__
      SET RESOURCES TO ".\EASYDLG.DLL"
   #ENDIF

   SELECT 0
   USE EASYDLG.DBF SHARED

   SELECT 0
   DBCREATE( cDBF, { { "ID"     , "N",  6, 0 }, ;
                     { "TYPE"   , "C", 20, 0 }, ;
                     { "TEXT"   , "C", 33, 0 }, ;
                     { "MODY1"  , "N",  6, 0 }, ;
                     { "MODY2"  , "N",  6, 0 }, ;
                     { "MODY3"  , "N",  6, 0 }, ;
                     { "MODY4"  , "N",  6, 0 }, ;
                     { "ADJUST1", "L",  1, 0 }, ;
                     { "ADJUST2", "L",  1, 0 }, ;
                     { "ADJUST3", "L",  1, 0 }, ;
                     { "ADJUST4", "L",  1, 0 }, ;
                     { "HCENTER", "L",  1, 0 }, ;
                     { "VCENTER", "L",  1, 0 }, ;
                     { "DISABLE", "L",  1, 0 }, ;
                     { "HIDE"   , "L",  1, 0 }, ;
                     { "BRWMODE", "L",  1, 0 }, ;
                     { "ORIG1"  , "N",  6, 0 }, ;
                     { "ORIG2"  , "N",  6, 0 }, ;
                     { "ORIG3"  , "N",  6, 0 }, ;
                     { "ORIG4"  , "N",  6, 0 }, ;
                     { "CURR1"  , "N",  6, 0 }, ;
                     { "CURR2"  , "N",  6, 0 }, ;
                     { "CURR3"  , "N",  6, 0 }, ;
                     { "CURR4"  , "N",  6, 0 } } )
   USE ( cDBF ) EXCLUSIVE
   ED_SetOneDlg( oAppDlg, NIL, .T. )
   EDTEMP->(DBGOTOP())

   DEFINE DIALOG oDlg NAME "EASYDIALOG" TITLE oAppDlg:cResName

   REDEFINE BUTTON aBtn[2] PROMPT ED_GL( "&OK" ) ID 101 OF oDlg ACTION oDlg:End()
   REDEFINE BUTTON PROMPT ED_GL( "&Set Original Values" ) ID 102 OF oDlg ACTION ;
      IIF( MsgNoYes( ED_GL( "Set original dialog and controls positions and sizes?" ), ED_GL("Please select") ), ;
           ( ED_SetDefault( oAppDlg, oBrw ), AEVAL( aUpd, {|x| x:Refresh() } ) ), .T. )

   REDEFINE BUTTON aBtn[1] ID 103 OF oDlg ;
      ACTION ED_SetStyle( oBrw, oDlg, aBtn,, oFld )

   REDEFINE LISTBOX oBrw ;
      FIELDS IIF( EDTEMP->ID = 0, "", STR( EDTEMP->ID, 6 ) ), ;
             EDTEMP->TYPE, ;
             EDTEMP->TEXT, ;
             STR( EDTEMP->MODY1, 6 ), ;
             STR( EDTEMP->MODY2, 6 ), ;
             STR( EDTEMP->MODY3, 6 ), ;
             STR( EDTEMP->MODY4, 6 ), ;
             IIF( EDTEMP->ADJUST1, cChecked, "" ), ;
             IIF( EDTEMP->ADJUST2, cChecked, "" ), ;
             IIF( EDTEMP->ADJUST3, cChecked, "" ), ;
             IIF( EDTEMP->ADJUST4, cChecked, "" ), ;
             IIF( EDTEMP->HCENTER, cChecked, "" ), ;
             IIF( EDTEMP->VCENTER, cChecked, "" ), ;
             IIF( EDTEMP->DISABLE, cChecked, "" ), ;
             IIF( EDTEMP->HIDE   , cChecked, "" ) ;
      HEADERS ED_GL( "ID" ) + " "     , ;
              " " + ED_GL( "Type" )   , ;
              " " + ED_GL( "Text" )   , ;
              ED_GL( "Top" ) + " "    , ;
              ED_GL( "Left" ) + " "   , ;
              ED_GL( "Width" ) + " "  , ;
              ED_GL( "Height" ) + " " , ;
              ED_GL( "Top" ) + " "    , ;
              ED_GL( "Left" ) + " "   , ;
              ED_GL( "Width" ) + " "  , ;
              ED_GL( "Height" ) + " " , ;
              ED_GL( "HCenter" ) + " ", ;
              ED_GL( "VCenter" ) + " ", ;
              ED_GL( "Disable" ) + " ", ;
              ED_GL( "Hide" ) + " " ;
      ID 151 OF oDlg ;
      FIELDSIZES 40, 74, 140, 42, 42, 42, 42, 42, 42, 42, 42, 48, 48, 48, 48 ;
      ON CHANGE ( ED_ShowControl( oAppDlg ), ;
                  AEVAL( aUpd, {|x| x:Refresh() } ), ;
                  IIF( UPPER( EDTEMP->TYPE ) = "DIALOG", ;
                       ( aUpd[12]:Hide(), aUpd[13]:Hide() ), ( aUpd[12]:Show(), aUpd[13]:Show() ) ), ;
                  IIF( ED_IsBrowse( EDTEMP->TYPE ), aUpd[14]:Show(), aUpd[14]:Hide() ) )

   oBrw:lCellStyle = .T.
   oBrw:aJustify   = { .T., .F., .F., .T., .T., .T., .T., .T., .T., .T., .T., .T., .T., .T., .T., .T. }
   oBrw:bLDblClick = { || ED_EditCell( oBrw, oAppDlg, aUpd ) }

   REDEFINE FOLDER oFld ID 110 OF oDlg ;
      PROMPT " " + ED_GL( "Position" ) + " / " + ED_GL( "Size" ) + " ", ;
             " " + ED_GL( "Others" ) + " " ;
      DIALOGS "EASYDIALOGFLD1", "EASYDIALOGFLD2"

   i := 1
   REDEFINE GET aUpd[1] VAR EDTEMP->MODY1 ID 201 OF oFld:aDialogs[i] VALID ED_Save2Refresh( oAppDlg )
   REDEFINE GET aUpd[2] VAR EDTEMP->MODY2 ID 202 OF oFld:aDialogs[i] VALID ED_Save2Refresh( oAppDlg )
   REDEFINE GET aUpd[3] VAR EDTEMP->MODY3 ID 203 OF oFld:aDialogs[i] VALID ED_Save2Refresh( oAppDlg )
   REDEFINE GET aUpd[4] VAR EDTEMP->MODY4 ID 204 OF oFld:aDialogs[i] VALID ED_Save2Refresh( oAppDlg )

   REDEFINE SAY aUpd[5] PROMPT ED_GL( "Adjust" ) ID 171 OF  oFld:aDialogs[i]

   REDEFINE SAY PROMPT ED_GL( "Position" ) + " / " + ED_GL( "Size" ) ID 172 OF  oFld:aDialogs[i]

   REDEFINE SAY PROMPT ED_GL( "Top"      ) + ":" ID 173 OF oFld:aDialogs[i]
   REDEFINE SAY PROMPT ED_GL( "Left"     ) + ":" ID 174 OF oFld:aDialogs[i]
   REDEFINE SAY PROMPT ED_GL( "Width"    ) + ":" ID 175 OF oFld:aDialogs[i]
   REDEFINE SAY PROMPT ED_GL( "Height"   ) + ":" ID 176 OF oFld:aDialogs[i]
   REDEFINE SAY PROMPT ED_GL( "Position" ) ID 177 OF oFld:aDialogs[i]
   REDEFINE SAY PROMPT ED_GL( "Size"     ) ID 178 OF oFld:aDialogs[i]

   REDEFINE CHECKBOX aUpd[6] VAR EDTEMP->ADJUST1 ID 211 OF oFld:aDialogs[i] ON CHANGE ED_Save2Refresh( oAppDlg )
   REDEFINE CHECKBOX aUpd[7] VAR EDTEMP->ADJUST2 ID 212 OF oFld:aDialogs[i] ON CHANGE ED_Save2Refresh( oAppDlg )
   REDEFINE CHECKBOX aUpd[8] VAR EDTEMP->ADJUST3 ID 213 OF oFld:aDialogs[i] ON CHANGE ED_Save2Refresh( oAppDlg )
   REDEFINE CHECKBOX aUpd[9] VAR EDTEMP->ADJUST4 ID 214 OF oFld:aDialogs[i] ON CHANGE ED_Save2Refresh( oAppDlg )

   //Position
   REDEFINE BTNBMP aCrtl[1] ID 301 OF oFld:aDialogs[i] RESOURCE "ARROW1" NOBORDER ;
      ACTION ( ED_Replace( "EDTEMP->MODY1", EDTEMP->MODY1 - nPosSteps ), ;
               ED_Replace( "EDTEMP->MODY2", EDTEMP->MODY2 - nPosSteps ), ;
               aUpd[1]:Refresh(), aUpd[2]:Refresh(), ED_Save2Refresh( oAppDlg ) )

   REDEFINE BTNBMP aCrtl[2] ID 302 OF oFld:aDialogs[i] RESOURCE "ARROW2" NOBORDER ;
      ACTION ( ED_Replace( "EDTEMP->MODY1", EDTEMP->MODY1 - nPosSteps ), ;
               aUpd[1]:Refresh(), ED_Save2Refresh( oAppDlg ) )

   REDEFINE BTNBMP aCrtl[3] ID 303 OF oFld:aDialogs[i] RESOURCE "ARROW3" NOBORDER ;
      ACTION ( ED_Replace( "EDTEMP->MODY1", EDTEMP->MODY1 - nPosSteps ), ;
               ED_Replace( "EDTEMP->MODY2", EDTEMP->MODY2 + nPosSteps ), ;
               aUpd[1]:Refresh(), aUpd[2]:Refresh(), ED_Save2Refresh( oAppDlg ) )

   REDEFINE BTNBMP aCrtl[4] ID 304 OF oFld:aDialogs[i] RESOURCE "ARROW4" NOBORDER ;
      ACTION ( ED_Replace( "EDTEMP->MODY2", EDTEMP->MODY2 + nPosSteps ), ;
               aUpd[2]:Refresh(), ED_Save2Refresh( oAppDlg ) )

   REDEFINE BTNBMP aCrtl[5] ID 305 OF oFld:aDialogs[i] RESOURCE "ARROW5" NOBORDER ;
      ACTION ( ED_Replace( "EDTEMP->MODY1", EDTEMP->MODY1 + nPosSteps ), ;
               ED_Replace( "EDTEMP->MODY2", EDTEMP->MODY2 + nPosSteps ), ;
               aUpd[1]:Refresh(), aUpd[2]:Refresh(), ED_Save2Refresh( oAppDlg ) )

   REDEFINE BTNBMP aCrtl[6] ID 306 OF oFld:aDialogs[i] RESOURCE "ARROW6" NOBORDER ;
      ACTION ( ED_Replace( "EDTEMP->MODY1", EDTEMP->MODY1 + nPosSteps ), ;
               aUpd[1]:Refresh(), ED_Save2Refresh( oAppDlg ) )

   REDEFINE BTNBMP aCrtl[7] ID 307 OF oFld:aDialogs[i] RESOURCE "ARROW7" NOBORDER ;
      ACTION ( ED_Replace( "EDTEMP->MODY1", EDTEMP->MODY1 + nPosSteps ), ;
               ED_Replace( "EDTEMP->MODY2", EDTEMP->MODY2 - nPosSteps ), ;
               aUpd[1]:Refresh(), aUpd[2]:Refresh(), ED_Save2Refresh( oAppDlg ) )

   REDEFINE BTNBMP aCrtl[8] ID 308 OF oFld:aDialogs[i] RESOURCE "ARROW8" NOBORDER ;
      ACTION ( ED_Replace( "EDTEMP->MODY2", EDTEMP->MODY2 - nPosSteps ), ;
               aUpd[2]:Refresh(), ED_Save2Refresh( oAppDlg ) )

   //Size
   REDEFINE BTNBMP aCrtl[9] ID 321 OF oFld:aDialogs[i] RESOURCE "ARROW1" NOBORDER ;
      ACTION ( ED_Replace( "EDTEMP->MODY4", EDTEMP->MODY4 - nSizeSteps ), ;
               ED_Replace( "EDTEMP->MODY3", EDTEMP->MODY3 - nSizeSteps ), ;
               aUpd[3]:Refresh(), aUpd[4]:Refresh(), ED_Save2Refresh( oAppDlg ) )

   REDEFINE BTNBMP aCrtl[10] ID 322 OF oFld:aDialogs[i] RESOURCE "ARROW2" NOBORDER ;
      ACTION ( ED_Replace( "EDTEMP->MODY4", EDTEMP->MODY4 - nSizeSteps ), ;
               aUpd[4]:Refresh(), ED_Save2Refresh( oAppDlg ) )

   REDEFINE BTNBMP aCrtl[11] ID 323 OF oFld:aDialogs[i] RESOURCE "ARROW3" NOBORDER ;
      ACTION ( ED_Replace( "EDTEMP->MODY4", EDTEMP->MODY4 - nSizeSteps ), ;
               ED_Replace( "EDTEMP->MODY3", EDTEMP->MODY3 + nSizeSteps ), ;
               aUpd[3]:Refresh(), aUpd[4]:Refresh(), ED_Save2Refresh( oAppDlg ) )

   REDEFINE BTNBMP aCrtl[12] ID 324 OF oFld:aDialogs[i] RESOURCE "ARROW4" NOBORDER ;
      ACTION ( ED_Replace( "EDTEMP->MODY3", EDTEMP->MODY3 + nSizeSteps ), ;
               aUpd[3]:Refresh(), ED_Save2Refresh( oAppDlg ) )

   REDEFINE BTNBMP aCrtl[13] ID 325 OF oFld:aDialogs[i] RESOURCE "ARROW5" NOBORDER ;
      ACTION ( ED_Replace( "EDTEMP->MODY4", EDTEMP->MODY4 + nSizeSteps ), ;
               ED_Replace( "EDTEMP->MODY3", EDTEMP->MODY3 + nSizeSteps ), ;
               aUpd[3]:Refresh(), aUpd[3]:Refresh(), ED_Save2Refresh( oAppDlg ) )

   REDEFINE BTNBMP aCrtl[14] ID 326 OF oFld:aDialogs[i] RESOURCE "ARROW6" NOBORDER ;
      ACTION ( ED_Replace( "EDTEMP->MODY4", EDTEMP->MODY4 + nSizeSteps ), ;
               aUpd[4]:Refresh(), ED_Save2Refresh( oAppDlg ) )

   REDEFINE BTNBMP aCrtl[15] ID 327 OF oFld:aDialogs[i] RESOURCE "ARROW7" NOBORDER ;
      ACTION ( ED_Replace( "EDTEMP->MODY4", EDTEMP->MODY4 + nSizeSteps ), ;
               ED_Replace( "EDTEMP->MODY3", EDTEMP->MODY3 - nSizeSteps ), ;
               aUpd[3]:Refresh(), aUpd[4]:Refresh(), ED_Save2Refresh( oAppDlg ) )

   REDEFINE BTNBMP aCrtl[16] ID 328 OF oFld:aDialogs[i] RESOURCE "ARROW8" NOBORDER ;
      ACTION ( ED_Replace( "EDTEMP->MODY3", EDTEMP->MODY3 - nSizeSteps ), ;
               aUpd[3]:Refresh(), ED_Save2Refresh( oAppDlg ) )

   REDEFINE GET aCrtl[17] VAR nPosSteps  ID 351 OF oFld:aDialogs[i] PICTURE "99"
   REDEFINE GET aCrtl[18] VAR nSizeSteps ID 352 OF oFld:aDialogs[i] PICTURE "99"

   i := 2
   REDEFINE CHECKBOX aUpd[10] VAR EDTEMP->HCENTER ID 401 OF oFld:aDialogs[i] ON CHANGE ED_Save2Refresh( oAppDlg )
   REDEFINE CHECKBOX aUpd[11] VAR EDTEMP->VCENTER ID 402 OF oFld:aDialogs[i] ON CHANGE ED_Save2Refresh( oAppDlg )

   REDEFINE CHECKBOX aUpd[12] VAR EDTEMP->DISABLE ID 411 OF oFld:aDialogs[i] ON CHANGE ED_Save2Refresh( oAppDlg )
   REDEFINE CHECKBOX aUpd[13] VAR EDTEMP->HIDE    ID 412 OF oFld:aDialogs[i] ON CHANGE ED_Save2Refresh( oAppDlg )

   REDEFINE CHECKBOX aUpd[14] VAR EDTEMP->BRWMODE ID 413 OF oFld:aDialogs[i] ON CHANGE ED_Save2Refresh( oAppDlg )

   ACTIVATE DIALOG oDlg ;
      ON INIT ( ED_SetStyle( oBrw, oDlg, aBtn, .F., oFld ), ;
                aUpd[12]:Hide(), aUpd[13]:Hide(), aUpd[14]:Hide(), ;
                aUpd[10]:SetText( ED_GL( "Center Horizontal" ) ), ;
                aUpd[11]:SetText( ED_GL( "Center Vertical" ) )  , ;
                aUpd[12]:SetText( ED_GL( "Disable" ) ), ;
                aUpd[13]:SetText( ED_GL( "Hide" ) ), ;
                aUpd[14]:SetText( ED_GL( "Adjust Cells on Resizing" ) ) )

   IF aShowCtrl <> NIL
      AEVAL( aShowCtrl, {|x| x:End() } )
   ENDIF

   EDTEMP->(DBCLOSEAREA())
   EASYDLG->(DBCLOSEAREA())
   ED_DelFile( cDBF )
   SELECT( nSelect )

   #IFNDEF __HARBOUR__
      SetResources( hOldRes )
   #ENDIF

RETURN NIL


*-- FUNCTION -----------------------------------------------------------------
*         Name: ED_SetStyle
* Beschreibung:
*    Argumente: None
* Return Value:                       Autor: Timm Sodtalbers
*-----------------------------------------------------------------------------
FUNCTION ED_SetStyle( oBrw, oDlg, aBtn, lChange, oFld )

  LOCAL i
  LOCAL nCurPos := oBrw:nColAct

  DEFAULT lChange := .T.

  IF lChange = .T.
     nEDStyle := IIF( nEDStyle = 0, 1, 0 )
  ENDIF

  IF nEDStyle = 0
     oBrw:SetFocus()
     FOR i := 1 TO nCurPos - 1
        oBrw:GoLeft()
     NEXT
     oBrw:Move( oBrw:nTop, oBrw:nLeft, 612, oBrw:nHeight, .T. )
     oDlg:Move( oDlg:nTop, oDlg:nLeft, 630, oDlg:nHeight, .T. )
     aBtn[2]:Move( aBtn[2]:nTop, 534, aBtn[2]:nWidth, aBtn[2]:nHeight, .T. )
     aBtn[1]:SetText( "<<<" )
     oFld:Hide()
  ELSE
     oBrw:SetFocus()
     FOR i := 1 TO nCurPos - 1
        oBrw:GoLeft()
     NEXT
     oBrw:Move( oBrw:nTop, oBrw:nLeft, 208, oBrw:nHeight, .T. )
     oDlg:Move( oDlg:nTop, oDlg:nLeft, 423, oDlg:nHeight, .T. )
     aBtn[2]:Move( aBtn[2]:nTop, 327, aBtn[2]:nWidth, aBtn[2]:nHeight, .T. )
     aBtn[1]:SetText( ">>>" )
     oFld:Show()
  ENDIF

  WndCenter( oDlg:hWnd )

RETURN NIL


*-- FUNCTION -----------------------------------------------------------------
*         Name: ED_ShowControl
* Beschreibung:
*    Argumente: None
* Return Value:                       Autor: Timm Sodtalbers
*-----------------------------------------------------------------------------
FUNCTION ED_ShowControl( oAppDlg )

   LOCAL nCtrlTop    := EDTEMP->CURR1
   LOCAL nCtrlLeft   := EDTEMP->CURR2
   LOCAL nCtrlWidth  := EDTEMP->CURR3
   LOCAL nCtrlHeight := EDTEMP->CURR4

   IF EDTEMP->HCENTER = .T.
      nCtrlLeft := oAppDlg:nWidth / 2 - nCtrlWidth / 2
   ENDIF
   IF EDTEMP->VCENTER = .T.
      nCtrlTop := oAppDlg:nHeight / 2 - nCtrlHeight / 2
   ENDIF

   IF aShowCtrl <> NIL
      AEVAL( aShowCtrl, {|x| x:End() } )
   ENDIF

   IF UPPER( EDTEMP->TYPE ) <> "DIALOG"
      aShowCtrl := ARRAY( 4 )
      // Left
      @ nCtrlTop - nHLightWidth, nCtrlLeft - nHLightWidth ;
         SAY aShowCtrl[1] PROMPT " " COLOR nHLightClr, nHLightClr PIXEL OF oAppDlg ;
         SIZE nHLightWidth, nCtrlHeight + nHLightWidth
      // Right
      @ nCtrlTop - nHLightWidth, nCtrlLeft + nCtrlWidth ;
         SAY aShowCtrl[2] PROMPT " " COLOR nHLightClr, nHLightClr PIXEL OF oAppDlg ;
         SIZE nHLightWidth, nCtrlHeight + nHLightWidth
      // Top
      @ nCtrlTop - nHLightWidth, nCtrlLeft - nHLightWidth ;
         SAY aShowCtrl[3] PROMPT " " COLOR nHLightClr, nHLightClr PIXEL OF oAppDlg ;
         SIZE nCtrlWidth + nHLightWidth, nHLightWidth
      // Bottom
      @ nCtrlTop + nCtrlHeight, nCtrlLeft - nHLightWidth ;
         SAY aShowCtrl[4] PROMPT " " COLOR nHLightClr, nHLightClr PIXEL OF oAppDlg ;
         SIZE nCtrlWidth + 2*nHLightWidth, nHLightWidth
   ENDIF

RETURN NIL


*-- FUNCTION -----------------------------------------------------------------
*         Name: ED_Save2Refresh
* Beschreibung:
*    Argumente: None
* Return Value:                       Autor: Timm Sodtalbers
*-----------------------------------------------------------------------------
FUNCTION ED_Save2Refresh( oDlg )

   LOCAL aDlgRect := ED_SaveDialog( oDlg )

   IF UPPER( EDTEMP->TYPE ) <> "DIALOG"
      REPLACE EDTEMP->CURR1 WITH EDTEMP->MODY1 + IIF( EDTEMP->ADJUST1, aDlgRect[1][4] - aDlgRect[2][4], 0 )
      REPLACE EDTEMP->CURR2 WITH EDTEMP->MODY2 + IIF( EDTEMP->ADJUST2, aDlgRect[1][3] - aDlgRect[2][3], 0 )
      REPLACE EDTEMP->CURR3 WITH EDTEMP->MODY3 + IIF( EDTEMP->ADJUST3, aDlgRect[1][3] - aDlgRect[2][3], 0 )
      REPLACE EDTEMP->CURR4 WITH EDTEMP->MODY4 + IIF( EDTEMP->ADJUST4, aDlgRect[1][4] - aDlgRect[2][4], 0 )
      ED_SaveRecno( oDlg )
   ENDIF

   ED_Refresh( oDlg )

RETURN (.T.)


*-- FUNCTION -----------------------------------------------------------------
*         Name: ED_Refresh
* Beschreibung:
*    Argumente: None
* Return Value:                       Autor: Timm Sodtalbers
*-----------------------------------------------------------------------------
FUNCTION ED_Refresh( oDlg )

   LOCAL i, nDlgTop, nDlgLeft, nDlgWidth, nDlgHeight
   LOCAL nRecNo     := EDTEMP->(RECNO())
   LOCAL lDialog    := ( UPPER( EDTEMP->TYPE ) = "DIALOG" )

   EDTEMP->(DBGOTOP())

   nDlgTop    := EDTEMP->MODY1
   nDlgLeft   := EDTEMP->MODY2
   nDlgWidth  := EDTEMP->MODY3
   nDlgHeight := EDTEMP->MODY4

   IF EDTEMP->HCENTER = .T.
      nDlgLeft := GetSysMetrics( 0 ) / 2 - nDlgWidth / 2
   ENDIF
   IF EDTEMP->VCENTER = .T.
      nDlgTop := GetSysMetrics( 1 ) / 2 - nDlgHeight / 2
   ENDIF

   oDlg:Move( nDlgTop, nDlgLeft, nDlgWidth, nDlgHeight, .T. )

   IF lDialog = .T.

      EDTEMP->(DBSKIP())

      DO WHILE .NOT. EDTEMP->(EOF())

         FOR i = 1 to Len( oDlg:aControls )
            IF oDlg:aControls[i]:nID = EDTEMP->ID
               ED_CtrlRefresh( oDlg, i )
            ENDIF
         NEXT

         EDTEMP->(DBSKIP())

      ENDDO

      EDTEMP->(DBGOTO( nRecNo ))

   ELSE

      EDTEMP->(DBGOTO( nRecNo ))

      FOR i = 1 to Len( oDlg:aControls )
         IF oDlg:aControls[i]:nID = EDTEMP->ID
            ED_CtrlRefresh( oDlg, i )
         ENDIF
      NEXT

   ENDIF

   ED_ShowControl( oDlg )

RETURN ( .T. )


*-- FUNCTION -----------------------------------------------------------------
*         Name: ED_CtrlRefresh
* Beschreibung:
*    Argumente: None
* Return Value:                       Autor: Timm Sodtalbers
*-----------------------------------------------------------------------------
FUNCTION ED_CtrlRefresh( oDlg, i )

   LOCAL nCtrlTop    := EDTEMP->CURR1
   LOCAL nCtrlLeft   := EDTEMP->CURR2
   LOCAL nCtrlWidth  := EDTEMP->CURR3
   LOCAL nCtrlHeight := EDTEMP->CURR4

   IF EDTEMP->HCENTER = .T.
      nCtrlLeft := oDlg:nWidth / 2 - nCtrlWidth / 2
   ENDIF
   IF EDTEMP->VCENTER = .T.
      nCtrlTop := oDlg:nHeight / 2 - nCtrlHeight / 2
   ENDIF

   oDlg:aControls[i]:Move( nCtrlTop, nCtrlLeft, nCtrlWidth, nCtrlHeight, .T. )

   IIF( EDTEMP->DISABLE, oDlg:aControls[i]:Disable(), oDlg:aControls[i]:Enable() )
   IIF( EDTEMP->HIDE   , oDlg:aControls[i]:Hide()   , oDlg:aControls[i]:Show() )

RETURN ( .T. )


*-- FUNCTION -----------------------------------------------------------------
*         Name: ED_SaveAll
* Beschreibung:
*    Argumente: None
* Return Value:                       Autor: Timm Sodtalbers
*-----------------------------------------------------------------------------
FUNCTION ED_SaveAll( oAppDlg )

   LOCAL nRecNo := EDTEMP->(RECNO())

   ED_SaveDialog( oAppDlg )

   EDTEMP->(DBGOTO( 2 ))

   DO WHILE .NOT. EDTEMP->(EOF())
      ED_SaveRecno( oAppDlg )
      EDTEMP->(DBSKIP())
   ENDDO

   EDTEMP->(DBGOTO( nRecNo ))

RETURN ( .T. )


*-- FUNCTION -----------------------------------------------------------------
*         Name: ED_SaveDialog
* Beschreibung:
*    Argumente: None
* Return Value:                       Autor: Timm Sodtalbers
*-----------------------------------------------------------------------------
FUNCTION ED_SaveDialog( oAppDlg )

   LOCAL aCurrRect, aOrigRect
   LOCAL nRecNo := EDTEMP->(RECNO())

   EDTEMP->(DBGOTOP())
   ED_WritePPString( oAppDlg:cResName, "Dialog", ;
               ALLTRIM(STR( EDTEMP->ORIG1, 10 )) + "|" + ;
               ALLTRIM(STR( EDTEMP->ORIG2, 10 )) + "|" + ;
               ALLTRIM(STR( EDTEMP->ORIG3, 10 )) + "|" + ;
               ALLTRIM(STR( EDTEMP->ORIG4, 10 )) + "|" + ;
               ALLTRIM(STR( EDTEMP->MODY1, 10 )) + "|" + ;
               ALLTRIM(STR( EDTEMP->MODY2, 10 )) + "|" + ;
               ALLTRIM(STR( EDTEMP->MODY3, 10 )) + "|" + ;
               ALLTRIM(STR( EDTEMP->MODY4, 10 )) + "|" + ;
               IIF( EDTEMP->ADJUST1, "1", "0" ) + "|" + ;
               IIF( EDTEMP->ADJUST2, "1", "0" ) + "|" + ;
               IIF( EDTEMP->ADJUST3, "1", "0" ) + "|" + ;
               IIF( EDTEMP->ADJUST4, "1", "0" ) + "|" + ;
               IIF( EDTEMP->HCENTER, "1", "0" ) + "|" + ;
               IIF( EDTEMP->VCENTER, "1", "0" ) + "|", ;
               cDlgIni )

   aCurrRect := { EDTEMP->MODY1, EDTEMP->MODY2, EDTEMP->MODY3, EDTEMP->MODY4 }
   aOrigRect := { EDTEMP->ORIG1, EDTEMP->ORIG2, EDTEMP->ORIG3, EDTEMP->ORIG4 }

   EDTEMP->(DBGOTO( nRecNo ))

RETURN ( { aCurrRect, aOrigRect } )


*-- FUNCTION -----------------------------------------------------------------
*         Name: ED_SaveRecno
* Beschreibung:
*    Argumente: None
* Return Value:                       Autor: Timm Sodtalbers
*-----------------------------------------------------------------------------
FUNCTION ED_SaveRecno( oAppDlg )

   LOCAL cRect

   ED_WritePPString( oAppDlg:cResName, ALLTRIM(STR( EDTEMP->ID, 6 )), ;
               ALLTRIM(STR( EDTEMP->ORIG1, 10 )) + "|" + ;
               ALLTRIM(STR( EDTEMP->ORIG2, 10 )) + "|" + ;
               ALLTRIM(STR( EDTEMP->ORIG3, 10 )) + "|" + ;
               ALLTRIM(STR( EDTEMP->ORIG4, 10 )) + "|" + ;
               ALLTRIM(STR( EDTEMP->CURR1, 10 )) + "|" + ;
               ALLTRIM(STR( EDTEMP->CURR2, 10 )) + "|" + ;
               ALLTRIM(STR( EDTEMP->CURR3, 10 )) + "|" + ;
               ALLTRIM(STR( EDTEMP->CURR4, 10 )) + "|" + ;
               ALLTRIM(STR( EDTEMP->MODY1, 10 )) + "|" + ;
               ALLTRIM(STR( EDTEMP->MODY2, 10 )) + "|" + ;
               ALLTRIM(STR( EDTEMP->MODY3, 10 )) + "|" + ;
               ALLTRIM(STR( EDTEMP->MODY4, 10 )) + "|" + ;
               IIF( EDTEMP->ADJUST1, "1", "0" ) + "|" + ;
               IIF( EDTEMP->ADJUST2, "1", "0" ) + "|" + ;
               IIF( EDTEMP->ADJUST3, "1", "0" ) + "|" + ;
               IIF( EDTEMP->ADJUST4, "1", "0" ) + "|" + ;
               IIF( EDTEMP->HCENTER, "1", "0" ) + "|" + ;
               IIF( EDTEMP->VCENTER, "1", "0" ) + "|" + ;
               IIF( EDTEMP->DISABLE, "1", "0" ) + "|" + ;
               IIF( EDTEMP->HIDE   , "1", "0" ) + "|" + ;
               ALLTRIM( EDTEMP->TYPE ) + "|" + ;
               ALLTRIM( EDTEMP->TEXT ) + "|" + ;
               IIF( ED_IsBrowse( EDTEMP->TYPE ), IIF( EDTEMP->BRWMODE, "1", "0" ) + "|", "" ), ;
               cDlgIni )

RETURN ( .T. )


*-- FUNCTION -----------------------------------------------------------------
*         Name: ED_EditCell
* Beschreibung:
*    Argumente: None
* Return Value:                       Autor: Timm Sodtalbers
*-----------------------------------------------------------------------------
FUNCTION ED_EditCell( oLbx, oAppDlg, aUpd )

   LOCAL nField := oLbx:nColAct

   IF nField >= 8 .AND. nField <= 15 .AND. ;
      .NOT. ( UPPER( EDTEMP->TYPE ) = "DIALOG" .AND. nField >= 14 .AND. nField <= 15 )

      REPLACE &( (oLbx:cAlias)->(FieldName( nField )) ) ;
         WITH !(oLbx:cAlias)->(FieldGet( nField ))
      oLbx:DrawSelect()
      oLbx:SetFocus()

      ED_Save2Refresh( oAppDlg )

   ELSEIF nField > 3 .AND. nField < 8

      oLbx:EditCol( nField, (oLbx:cAlias)->( FieldGet( nField ) ),, ;
                    {|v,k| ED_CellVarPut( v, nField, oLbx ), ;
                           ED_Save2Refresh( oAppDlg ) } )

   ENDIF

   AEVAL( aUpd, {|x| x:Refresh() } )

RETURN ( NIL )


*-- FUNCTION -----------------------------------------------------------------
* Name........: ED_CellVarPut
* Beschreibung:
* Argumente...: None
* R�ckgabewert: .T.
* Author......: Timm Sodtalbers
*-----------------------------------------------------------------------------
FUNCTION ED_CellVarPut( xVar, nField, oLbx )

   ( oLbx:cAlias )->( FieldPut( nField, xVar ) )

   oLbx:DrawSelect()
   oLbx:SetFocus()

RETURN (.T.)


*-- FUNCTION -----------------------------------------------------------------
* Name........: ED_SetDefault
* Beschreibung:
* Argumente...: None
* R�ckgabewert: .T.
* Author......: Timm Sodtalbers
*-----------------------------------------------------------------------------
FUNCTION ED_SetDefault( oAppDlg, oBrw )

   LOCAL nRecNo := EDTEMP->(RECNO())

   EDTEMP->(DBGOTOP())

   DO WHILE .NOT. EDTEMP->(EOF())

      REPLACE EDTEMP->CURR1 WITH EDTEMP->ORIG1, ;
              EDTEMP->CURR2 WITH EDTEMP->ORIG2, ;
              EDTEMP->CURR3 WITH EDTEMP->ORIG3, ;
              EDTEMP->CURR4 WITH EDTEMP->ORIG4, ;
              EDTEMP->MODY1 WITH EDTEMP->ORIG1, ;
              EDTEMP->MODY2 WITH EDTEMP->ORIG2, ;
              EDTEMP->MODY3 WITH EDTEMP->ORIG3, ;
              EDTEMP->MODY4 WITH EDTEMP->ORIG4

      EDTEMP->(DBSKIP())

   ENDDO

   EDTEMP->(DBGOTOP())

   IF oBrw <> NIL
      oBrw:Refresh()
   ENDIF

   ED_SaveAll( oAppDlg )
   ED_Refresh( oAppDlg )

RETURN NIL


*-- FUNCTION -----------------------------------------------------------------
* Name........: ED_GetOrigRes
* Beschreibung:
* Argumente...: None
* R�ckgabewert: .T.
* Author......: Timm Sodtalbers
*-----------------------------------------------------------------------------
FUNCTION ED_AdjustToRes( cOrigDlgIni, cMessage )

   LOCAL i, y, aHRes[3], aVRes[3], cDef, aDialogs, aIniEntries, cEntry, cValue
   LOCAL aNewSize[4]
   LOCAL nCurrHRes  := GetSysMetrics( 0 )
   LOCAL nCurrVRes  := GetSysMetrics( 1 )
   LOCAL cEDSection := "EasyDialogGeneral"

   PUBLIC cDlgIni

   DEFAULT cMessage    := ""
   DEFAULT cOrigDlgIni := ".\EasyDlg.ini"

   ED_GetConfig( cOrigDlgIni )

   cDef := GetPvProfString( cEDSection, "Resolution", "", cDlgIni )

   IF EMPTY( cDef )
      cDef := REPLICATE( ALLTRIM(STR( nCurrHRes, 10 )) + "|" + ;
                         ALLTRIM(STR( nCurrVRes, 10 )) + "|", 2 )
      ED_WritePPString( cEDSection, "Resolution", cDef, cDlgIni )
   ENDIF

   aHRes[1] := VAL( StrToken( cDef, 1, "|" ) )
   aVRes[1] := VAL( StrToken( cDef, 2, "|" ) )
   aHRes[2] := VAL( StrToken( cDef, 3, "|" ) )
   aVRes[2] := VAL( StrToken( cDef, 4, "|" ) )

   IF aHRes[2] <> nCurrHRes .OR. aVRes[2] <> nCurrVRes

      IF .NOT. EMPTY( cMessage )
         MsgInfo( cMessage, ED_GL( "Information", .T. ) )
      ENDIF

      aDialogs := ED_GetSectNames( cDlgIni )

      FOR i := 1 TO LEN( aDialogs )

         IF aDialogs[i] <> cEDSection

            aIniEntries := ED_GetIniSection( aDialogs[i], cDlgIni, .F. )

            FOR y := 1 TO LEN( aIniEntries )

               cEntry := StrToken( aIniEntries[y], 1, "=" )
               cValue := StrToken( aIniEntries[y], 2, "=" )

               IF UPPER( cEntry ) = "DIALOG"

                  aNewSize[1] := VAL( StrToken( cValue, 5, "|" ) )
                  aNewSize[2] := VAL( StrToken( cValue, 6, "|" ) )
                  aNewSize[3] := VAL( StrToken( cValue, 7, "|" ) )
                  aNewSize[4] := VAL( StrToken( cValue, 8, "|" ) )

                  IF VAL( StrToken( cValue,  9, "|" ) ) = 1
                     aNewSize[1] := aNewSize[1] / aVRes[2] * nCurrVRes
                  ENDIF
                  IF VAL( StrToken( cValue, 10, "|" ) ) = 1
                     aNewSize[2] := aNewSize[2] / aHRes[2] * nCurrHRes
                  ENDIF
                  IF VAL( StrToken( cValue, 11, "|" ) ) = 1
                     aNewSize[3] := aNewSize[3] / aHRes[2] * nCurrHRes
                  ENDIF
                  IF VAL( StrToken( cValue, 12, "|" ) ) = 1
                     aNewSize[4] := aNewSize[4] / aVRes[2] * nCurrVRes
                  ENDIF

                  ED_WritePPString( aDialogs[i], cEntry, ;
                     SUBSTR( cValue, 1, ED_StrAtNum( "|", cValue, 4 ) ) + ;
                     ALLTRIM(STR( aNewSize[1], 10 )) + "|" + ;
                     ALLTRIM(STR( aNewSize[2], 10 )) + "|" + ;
                     ALLTRIM(STR( aNewSize[3], 10 )) + "|" + ;
                     ALLTRIM(STR( aNewSize[4], 10 )) + "|" + ;
                     StrToken( cValue,  9, "|" ) + "|" + ;
                     StrToken( cValue, 10, "|" ) + "|" + ;
                     StrToken( cValue, 11, "|" ) + "|" + ;
                     StrToken( cValue, 12, "|" ) + "|" + ;
                     StrToken( cValue, 13, "|" ) + "|" + ;
                     StrToken( cValue, 14, "|" ) + "|1|", ;
                     cDlgIni )

               ENDIF

            NEXT

         ENDIF

      NEXT

      ED_WritePPString( cEDSection, "Resolution", ;
                        StrToken( cDef, 1, "|" )      + "|" + ;
                        StrToken( cDef, 2, "|" )      + "|" + ;
                        ALLTRIM(STR( nCurrHRes, 10 )) + "|" + ;
                        ALLTRIM(STR( nCurrVRes, 10 )) + "|", ;
                        cDlgIni )
   ENDIF

RETURN NIL


*-- FUNCTION -----------------------------------------------------------------
* Name........: ED_GetIniSection
* Beschreibung:
* Argumente...: None
* R�ckgabewert: .T.
* Author......: Timm Sodtalbers
*-----------------------------------------------------------------------------
FUNCTION ED_GetIniSection( cSection, cIniFile, lSort )

   LOCAL p
   LOCAL aEntries := {}
   LOCAL nBuffer  :=  32000 //8192
   LOCAL cBuffer  := Space( nBuffer )

   DEFAULT lSort := .T.

   if At( ".", cIniFile ) == 0
      cIniFile += ".ini"
   endif

   ED_GetPPSection( cSection, @cBuffer, nBuffer, cIniFile )

   WHILE ( p := At( Chr( 0 ), cBuffer ) ) > 1
      AAdd( aEntries, Left( cBuffer, p - 1 ) )
      cBuffer = SubStr( cBuffer, p + 1 )
   ENDDO

   IF lSort = .T.
      ASORT( aEntries,,, {|x,y| VAL( SUBSTR( x, 1, AT( "=", x ) - 1 ) ) < ;
                                VAL( SUBSTR( y, 1, AT( "=", y ) - 1 ) ) } )
   ENDIF

RETURN aEntries


*-- FUNCTION -----------------------------------------------------------------
* Name........: ED_GetSectNames
* Beschreibung:
* Argumente...: None
* R�ckgabewert: .T.
* Author......: Timm Sodtalbers
*-----------------------------------------------------------------------------
FUNCTION ED_GetSectNames( cIniFile )

   LOCAL p
   LOCAL aEntries := {}
   LOCAL nBuffer  :=  32000 //8192
   LOCAL cBuffer  := Space( nBuffer )

   if At( ".", cIniFile ) == 0
      cIniFile += ".ini"
   endif

   ED_GetPPSNames( @cBuffer, nBuffer, cIniFile )

   WHILE ( p := At( Chr( 0 ), cBuffer ) ) > 1
      AAdd( aEntries, Left( cBuffer, p - 1 ) )
      cBuffer = SubStr( cBuffer, p + 1 )
   ENDDO

RETURN aEntries


*-- FUNCTION -----------------------------------------------------------------
* Name........: ED_GetIniEntry
* Beschreibung:
* Argumente...: None
* R�ckgabewert: .T.
* Author......: Timm Sodtalbers
*-----------------------------------------------------------------------------
FUNCTION ED_GetIniEntry( aEntries, cEntry, uDefault, uVar, nIndex )

   LOCAL cType  := ValType( If( uDefault != nil, uDefault, uVar ) )

   IF nIndex = NIL
      nIndex := ASCAN( aEntries, {|x| SUBSTR( x, 1, AT( "=", x ) - 1 ) == cEntry } )
   ENDIF

   IF nIndex = 0

      IF uDefault = nil
         uVar := ""
      ELSE
         uVar := uDefault
      ENDIF

   ELSE

      uVar := aEntries[ nIndex ]
      uVar := SUBSTR( uVar, AT( "=", uVar ) + 1 )

      DO CASE
      CASE cType == "N"
         uVar = VAL( uVar )
      CASE cType == "D"
         uVar = CToD( uVar )
      CASE cType == "L"
         uVar = ( Upper( uVar ) == ".T." )
      endcase

   ENDIF

RETURN uVar


*-- FUNCTION -----------------------------------------------------------------
* Name........: ED_Replace
* Beschreibung:
* Argumente...: None
* R�ckgabewert: .T.
* Author......: Timm Sodtalbers
*-----------------------------------------------------------------------------
FUNCTION ED_Replace( cReplFeld, xAusdruck )

   REPLACE &cReplFeld with xAusdruck

RETURN (.T.)


*-- FUNCTION -----------------------------------------------------------------
* Name........: ED_StrAtNum( <cSearch>, <cString>, <nCount> )
* Beschreibung: n-tes Auftreten einer Zeichenfolge in Strings ermitteln
*               StrAtNum() sucht das <nCount>-te Auftreten von <cSearch>
*               in <cString>. War die Suche erfolgreich, wird die Position
*               innerhalb <cString> zur�ckgegeben, andernfalls 0.
* R�ckgabewert: die Position des <nCount>-ten Auftretens von <cSearch>.
* Author......: Timm Sodtalbers
*-----------------------------------------------------------------------------
FUNCTION ED_StrAtNum( cSearch, cString, nNr )

   cString := STRTRAN( cString, cSearch, REPLICATE( "@", LEN( cSearch ) ),, nNr - 1 )

RETURN AT( cSearch, cString )


*-- FUNCTION -----------------------------------------------------------------
* Name........: ED_GetColor
* Beschreibung:
* Argumente...: None
* R�ckgabewert: .T.
* Author......: Timm Sodtalbers
*-----------------------------------------------------------------------------
FUNCTION ED_GetColor( cColor )

   LOCAL nColor := 0

   IF AT( ",", cColor ) <> 0
      nColor := RGB( VAL(StrToken( cColor, 1, "," )), ;
                     VAL(StrToken( cColor, 2, "," )), ;
                     VAL(StrToken( cColor, 3, "," )) )
   ELSE
      nColor := VAL( cColor )
   ENDIF

RETURN ( nColor )


*-- FUNCTION -----------------------------------------------------------------
* Name........: ED_LF2SF
* Beschreibung: Long file to short file with path
* Argumente...: None
* R�ckgabewert: .T.
* Author......: Timm Sodtalbers
*-----------------------------------------------------------------------------
FUNCTION ED_LF2SF( cFile )

   #IFDEF __HARBOUR__
      RETURN( cFile )
   #ELSE
      #IFDEF __XPP__
         RETURN( cFile )
      #ELSE
         RETURN IIF( EMPTY( cFile ), "", ED_LPN2SPN( ED_GetFullPath( ALLTRIM( cFile ) ) ) )
      #ENDIF
   #ENDIF

RETURN NIL


#IFNDEF __HARBOUR__
#IFNDEF __XPP__

*-- FUNCTION -----------------------------------------------------------------
* Name........: ED_LPN2SPN
* Beschreibung:
* Argumente...: None
* R�ckgabewert: .T.
* Author......: Timm Sodtalbers
*-----------------------------------------------------------------------------
FUNCTION ED_LPN2SPN( cLPN )

   LOCAL cSPN:="", I, II := 1
   LOCAL lIsLastBackSlash := .T.

   IF RAT( "\", cLPN ) < LEN( ALLTRIM( cLPN ) )
      cLPN = ALLTRIM( cLPN ) + "\"
      lIsLastBackSlash := .F.
   ENDIF

   IF SUBSTR( cLPN, 2, 1 ) = ":"
      cSPN += SUBSTR( cLPN, 1, AT( "\", cLPN ) )
      ii := 2
   ENDIF

   FOR I := 4 TO LEN( cLPN )
      IF SUBSTR( cLPN, I, 1 ) = "\"
         cSPN += ED_LFN2SFN( cSPN + STRTOKEN( cLPN, ii, "\" ) )
         cSPN += "\"
         ii++
      ENDIF
   NEXT

   IF !lIsLastBackSlash
      cSPN := SUBSTR( cSPN, 1, LEN( cSPN ) -1 )
   ENDIF

RETURN cSPN


*-- FUNCTION -----------------------------------------------------------------
* Name........: ED_LFN2SFN
* Beschreibung: Long File Name to Short File Name (works with short too)
*               Guarantee a short file name (path not expanded)
*-----------------------------------------------------------------------------
FUNCTION ED_LFN2SFN( cSpec )

   LOCAL oWin32, c, h

   STRUCT oWin32
      MEMBER nFileAttributes  AS DWORD
      MEMBER nCreation        AS STRING LEN 8
      MEMBER nLastRead        AS STRING LEN 8
      MEMBER nLastWrite       AS STRING LEN 8
      MEMBER nSizeHight       AS DWORD
      MEMBER nSizeLow         AS DWORD
      MEMBER nReserved0       AS DWORD
      MEMBER nReserved1       AS DWORD
      MEMBER cFileName        AS STRING LEN 260
      MEMBER cAltName         AS STRING LEN  14
   ENDSTRUCT

   c := oWin32:cBuffer
   h := ED_FindFst(cSpec,@c)
   oWin32:cBuffer := c

   ED_FindCls(h)

RETURN if(empty(ED_psz(oWin32:cAltName)),ED_psz(oWin32:cFileName),ED_psz(oWin32:cAltName))


*-- FUNCTION -----------------------------------------------------------------
*         Name: ED_psz
*  Description: Truncate a zero-terminated string to a proper size
*    Arguments: cZString - string containing zeroes
* Return Value: cString  - string without zeroes
*-----------------------------------------------------------------------------
FUNCTION ED_psz(c)
RETURN substr(c,1,at(chr(0),c)-1)


*-- FUNCTION -----------------------------------------------------------------
* Name........: ED_GetFullPath
* Beschreibung: Short File Name to Long Path Name (works with long too)
*               Returns a complete, LONG pathname and LONG filename.
*-----------------------------------------------------------------------------
Function ED_GetFullPath( cSpec )

   LOCAL cLongName := Space(261)
   LOCAL nNamePos  := 0

   ED_FullPathName( cSpec, Len( cLongName ), @cLongName, @nNamePos )

RETURN ALLTRIM( cLongName )

#ENDIF
#ENDIF


*-- FUNCTION -----------------------------------------------------------------
* Name........: ED_GetTempPath
* Description.:
* Parameters..:
* Return value: the windows temp directory
* Author......: Timm Sodtalbers
*-----------------------------------------------------------------------------
FUNCTION ED_GetTempPath()

#IFDEF __HARBOUR__

   LOCAL cDir := GetWinDir()

   IF RIGHT( cDir, 1 ) == "\"
      cDir = SUBSTR( cDir, 1, LEN( cDir ) - 1 )
   ENDIF

   IF lIsDir( cDir + "\temp" )
      cDir += "\temp"
   ELSEIF lIsDir( cDir + "\tmp" )
      cDir += "\tmp"
   ENDIF

#ELSE

   LOCAL cDir := GetEnv("TEMP")

   IF EMPTY( cDir )
      cDir := GetEnv("TMP")
   ENDIF

   IF RIGHT( cDir, 1 ) == "\"
      cDir = SUBSTR( cDir, 1, LEN( cDir ) - 1 )
   ENDIF

   IF !EMPTY( cDir )
      IF !lIsDir( cDir )
         cDir := GetWinDir()
      ENDIF
   ELSE
      cDir := GetWinDir()
   ENDIF

#ENDIF

RETURN ( cDir )


*-- FUNCTION -----------------------------------------------------------------
* Name........: ED_GL
* Beschreibung: Get Language
* Argumente...: English string
* R�ckgabewert: translated string
* Author......: Timm Sodtalbers
*-----------------------------------------------------------------------------
FUNCTION ED_GL( cOriginal, lOpenDBF )

   LOCAL cAltText   := strtran( cOriginal, " ", "_" )
   LOCAL cText      := cAltText
   LOCAL nAltSelect := Select()

   DEFAULT lOpenDBF := .F.

   IF lOpenDBF = .T.
      SELECT 0
      USE EASYDLG.DBF SHARED
   ELSE
      SELECT EASYDLG
   ENDIF

   //fill the language file
   IF nLanguage = 0 .OR. nLanguage > 1

      GO TOP
      LOCATE FOR ALLTRIM( EASYDLG->LANGUAGE1 ) == ALLTRIM( cText )

      IF EASYDLG->(EOF())
         FLOCK()
         APPEND BLANK
         REPLACE EASYDLG->LANGUAGE1 WITH cText
         UNLOCK
      ENDIF

   ENDIF

   IF nLanguage > 1
      cText := EVAL( EASYDLG->( FieldBlock( "LANGUAGE" + ALLTRIM(STR(nLanguage, 2)) ) ) )
   ENDIF

   IF EMPTY( cText ) .OR. nLanguage = 1
      cText := cAltText
   ENDIF

   IF lOpenDBF = .T.
      EASYDLG->(DBCLOSEAREA())
   ELSE
      SELECT EASYDLG
   ENDIF

   SELECT( nAltSelect )

RETURN ( STRTRAN(ALLTRIM( cText ), "_", " " ) )


*-- FUNCTION -----------------------------------------------------------------
* Name........: ED_xBrowse
* Beschreibung:
* Argumente...:
* R�ckgabewert:
* Author......: Timm Sodtalbers
*-----------------------------------------------------------------------------
FUNCTION ED_xBrowse( lValue )

   lxBrowse := lValue

RETURN NIL


*-- FUNCTION -----------------------------------------------------------------
* Name........: ED_xBrwGetColSizes
* Beschreibung:
* Argumente...:
* R�ckgabewert:
* Author......: Timm Sodtalbers
*-----------------------------------------------------------------------------
FUNCTION ED_xBrwGetColSizes( oBrw )

   LOCAL aSizes := {}

   AEVAL( oBrw:aCols, {|x| AADD( aSizes, x:nWidth ) } )

RETURN aSizes


*-- FUNCTION -----------------------------------------------------------------
* Name........: ED_xBrwSetColSizes
* Beschreibung:
* Argumente...:
* R�ckgabewert:
* Author......: Timm Sodtalbers
*-----------------------------------------------------------------------------
FUNCTION ED_xBrwSetColSizes( oBrw, aSizes )

   AEVAL( aSizes, {|x,y| oBrw:aCols[y]:nWidth := x } )
   oBrw:Refresh()

RETURN NIL


*-- FUNCTION -----------------------------------------------------------------
* Name........: ED_MsgDemo
* Beschreibung:
*-----------------------------------------------------------------------------
FUNCTION ED_MsgDemo( cText, cCaption, nType )
RETURN ED_APIMseBoxEx( GetActiveWindow(), cText, cCaption, nType, nil )

DLL32 FUNCTION ED_APIMseBoxEx( hWnd AS LONG, lpText AS STRING, ;
               lpCaption AS STRING, uType AS LONG, wLanguageId AS LONG ) ;
               AS LONG PASCAL FROM "MessageBoxExA" LIB "User32.dll"

DLL32 FUNCTION ED_FullPathName( lpszFile AS LPSTR, cchPath AS DWORD,;
               lpszPath AS LPSTR, @nFilePos AS PTR ) AS DWORD ;
               PASCAL FROM "GetFullPathNameA" LIB "kernel32.dll"

DLL32 FUNCTION ED_FindFst( lpFilename AS LPSTR, @cWin32DataInfo AS LPSTR ) AS LONG PASCAL ;
   FROM "FindFirstFileA" LIB "KERNEL32.DLL"

DLL32 FUNCTION ED_FindCls(nHandle AS LONG) AS BOOL PASCAL ;
   FROM "FindClose" LIB "KERNEL32.DLL"

DLL32 FUNCTION ED_GetPPSection( cSection AS LPSTR, @cData AS LPSTR, ;
                             nSize AS DWORD, cFile AS LPSTR ) ;
   AS DWORD PASCAL ;
   FROM "GetPrivateProfileSectionA" ;
   LIB "Kernel32.dll"

DLL32 FUNCTION ED_WritePPString( cSection AS LPSTR, cKeyName AS LPSTR, ;
                                 cString AS LPSTR, cFile AS LPSTR ) ;
   AS DWORD PASCAL ;
   FROM "WritePrivateProfileStringA" ;
   LIB "Kernel32.dll"

DLL32 FUNCTION ED_GetPPSNames( @cData AS LPSTR, nSize AS DWORD, cFile AS LPSTR ) ;
   AS DWORD PASCAL ;
   FROM "GetPrivateProfileSectionNamesA" ;
   LIB "Kernel32.dll"

DLL32 FUNCTION ED_DelFile( cFileName AS LPSTR ) ;
   AS BOOL PASCAL FROM "DeleteFileA" LIB "kernel32.dll"

DLL32 FUNCTION ED_CreateFile( cFileName AS LPSTR, ;
                              dwDesiredAccess AS LONG, ;
                              dwShareMode AS LONG, ;
                              lpSecurityAttributes AS LONG, ;
                              dwCreationDisposition AS LONG, ;
                              dwFlagsAndAttributes AS LONG, ;
                              hTemplateFile AS LONG ) ;
   AS LONG FROM "CreateFileA" LIB "kernel32.dll"