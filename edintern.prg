
#INCLUDE "FiveWin.ch"

MEMVAR cDlgIni

*-- FUNCTION -----------------------------------------------------------------
* Name........: ED_ResizeOneDlg
* Beschreibung:
* Argumente...: None
* R�ckgabewert: .T.
* Author......: Timm Sodtalbers
*-----------------------------------------------------------------------------
FUNCTION ED_ResizeOneDlg( oDlg, lRefresh, lMove, cUserResName )

   LOCAL i, oRect2, cRect2, cDef, cNewDef, cClass, cTitle, cRect, aIniEntries
   LOCAL cDlgIniLong, hFile, cAdjust, nOldWidth
   LOCAL aOrigRect[4], aCurrRect[4], aCurrControl[4]
   LOCAL nAdjustTop, nAdjustLeft, nAdjustWidth, nAdjustHeight
   LOCAL cCtrls  := ""
   LOCAL oRect   := oDlg:GetRect()

   DEFAULT lRefresh     := .T.
   DEFAULT lMove        := .F.
   DEFAULT cUserResName := oDlg:cResName

   aCurrRect[1] := oRect:nTop
   aCurrRect[2] := oRect:nLeft
   aCurrRect[3] := oDlg:nWidth
   aCurrRect[4] := oDlg:nHeight

   cRect := ALLTRIM(STR( aCurrRect[1], 10 )) + "|" + ;
            ALLTRIM(STR( aCurrRect[2], 10 )) + "|" + ;
            ALLTRIM(STR( aCurrRect[3], 10 )) + "|" + ;
            ALLTRIM(STR( aCurrRect[4], 10 )) + "|"

   IF EMPTY( GetPvProfString( cUserResName, "Dialog", "", cDlgIni ) )
      ED_WritePPString( cUserResName, "Dialog", cRect + cRect + "1|1|1|1|", cDlgIni )
   ENDIF

   cDef := GetPvProfString( cUserResName, "Dialog", "", cDlgIni )
   aOrigRect[1] := VAL( StrToken( cDef, 1, "|" ) )
   aOrigRect[2] := VAL( StrToken( cDef, 2, "|" ) )
   aOrigRect[3] := VAL( StrToken( cDef, 3, "|" ) )
   aOrigRect[4] := VAL( StrToken( cDef, 4, "|" ) )

   nAdjustTop    := VAL( StrToken( cDef,  9, "|" ) )
   nAdjustLeft   := VAL( StrToken( cDef, 10, "|" ) )
   nAdjustWidth  := VAL( StrToken( cDef, 11, "|" ) )
   nAdjustHeight := VAL( StrToken( cDef, 12, "|" ) )

   cRect := ALLTRIM(STR( IIF( nAdjustTop    = 1, aCurrRect[1], aOrigRect[1] ), 10 )) + "|" + ;
            ALLTRIM(STR( IIF( nAdjustLeft   = 1, aCurrRect[2], aOrigRect[2] ), 10 )) + "|" + ;
            ALLTRIM(STR( IIF( nAdjustWidth  = 1, aCurrRect[3], aOrigRect[3] ), 10 )) + "|" + ;
            ALLTRIM(STR( IIF( nAdjustHeight = 1, aCurrRect[4], aOrigRect[4] ), 10 )) + "|"

   ED_WritePPString( cUserResName, "Dialog", ;
            SUBSTR( cDef, 1, ED_StrAtNum( "|", cDef, 4 ) ) + ;
            cRect + ;
            STR( nAdjustTop   , 1 ) + "|" + ;
            STR( nAdjustLeft  , 1 ) + "|" + ;
            STR( nAdjustWidth , 1 ) + "|" + ;
            STR( nAdjustHeight, 1 ) + "|" + ;
            StrToken( cDef, 13, "|" ) + "|" + ;
            StrToken( cDef, 14, "|" ) + "|0|", ;
            cDlgIni )

   IF lMove = .T.
      RETURN(.T.)
   ENDIF

   aIniEntries := ED_GetIniSection( cUserResName, cDlgIni, .F. )

   FOR i = 1 to Len( oDlg:aControls )

      oRect2 := oDlg:aControls[i]:GetRect()
      cRect2 := ALLTRIM(STR( oDlg:aControls[i]:nTop   , 6 )) + "|" + ;
                ALLTRIM(STR( oDlg:aControls[i]:nLeft  , 6 )) + "|" + ;
                ALLTRIM(STR( oDlg:aControls[i]:nWidth , 6 )) + "|" + ;
                ALLTRIM(STR( oDlg:aControls[i]:nHeight, 6 )) + "|"

      cDef := ED_GetIniEntry( aIniEntries, ALLTRIM(STR( oDlg:aControls[i]:nID, 10 )), "" )

      nOldWidth := VAL( StrToken( cDef, 7, "|" ) )

      IF EMPTY( cDef )
         //insert control if not found
         cDef := cRect2 + cRect2 + cRect2
         ED_WritePPString( cUserResName, ALLTRIM(STR( oDlg:aControls[i]:nID, 10 )), ;
                     cDef, cDlgIni )
      ENDIF

      cClass := oDlg:aControls[i]:ClassName()
      DEFAULT cClass := ""

      aCurrControl := ED_SetCurrValues( cDef, aCurrRect, aOrigRect )

      cRect2 := ALLTRIM(STR( aCurrControl[1], 10 )) + "|" + ;
                ALLTRIM(STR( aCurrControl[2], 10 )) + "|" + ;
                ALLTRIM(STR( aCurrControl[3], 10 )) + "|" + ;
                ALLTRIM(STR( aCurrControl[4], 10 )) + "|"

      cTitle := ALLTRIM( STRTRAN( oDlg:aControls[i]:cTitle, CRLF, "" ) )
      cTitle := ALLTRIM( PADR( cTitle, 30 ) ) + IIF( LEN( cTitle ) > 30, "...", "" )

      cAdjust := SUBSTR( cDef, ED_StrAtNum( "|", cDef, 12 )+1, ;
                               ED_StrAtNum( "|", cDef, 20 ) - ED_StrAtNum( "|", cDef, 12 ) )
      IF EMPTY( cAdjust )
         cAdjust := "0|0|0|0|0|0|0|0|"
      ENDIF

      cNewDef := SUBSTR( cDef, 1, ED_StrAtNum( "|", cDef, 4 ) ) + ;
                 cRect2 + ;
                 StrToken( cDef,  9, "|" ) + "|" + ;
                 StrToken( cDef, 10, "|" ) + "|" + ;
                 StrToken( cDef, 11, "|" ) + "|" + ;
                 StrToken( cDef, 12, "|" ) + "|" + ;
                 cAdjust + ;
                 cClass + "|" + ;
                 cTitle + "|" + ;
                 IIF( ED_IsBrowse( cClass ), StrToken( cDef, 23, "|" ) + "|", "" )

      IF lRefresh := .T.

         IF StrToken( cDef, 17, "|" ) = "1"
            aCurrControl[2] := aCurrRect[3] / 2 - aCurrControl[3] / 2
         ENDIF
         IF StrToken( cDef, 18, "|" ) = "1"
            aCurrControl[1] := aCurrRect[4] / 2 - aCurrControl[4] / 2
         ENDIF

         oDlg:aControls[i]:Move( aCurrControl[1], aCurrControl[2], ;
                                 aCurrControl[3], aCurrControl[4], .T. )

      ENDIF

      cNewDef := ED_AdjustBrowse( oDlg, cNewDef, i, nOldWidth )

      ED_WritePPString( cUserResName, ALLTRIM(STR( oDlg:aControls[i]:nID, 10 )), ;
                        cNewDef, cDlgIni )

   NEXT

   oDlg:Refresh()

RETURN (.T.)